# vx0f.com Frontend

![vx0f.com](assets/vx0f_logo_1.png)

I'm only putting this here as a quick test of GitLab. It will also get me familiar with how Gitlab works. I've been meaning to mess around with it this year. Earlier this year (2018), I ran an instance of GitLab at home. It was too heavy for my old Athlon X2. I ended up moving to Gitea for my home instance.

# Frontend

## Bulma

I stumbled upon Bulma a little while ago. It promised to make my life easier. I guess we will see. So far nonsense has ensued. I'll chalk it up to me not fully understanding anything.

# License

![](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons License")
This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Fork it, Fuck It Up, Just don't call my name if you get in trouble.
